package com.example.xmppexample;


import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Localpart;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Xmpp extends Thread {

    public static final int PORT = 5222;
    private XMPPTCPConnection connection;
    private String HOST = "192.168.1.24";
    private boolean user_already_created;
    private String username;
    private String username2;
    private JSONObject js2;

    public Xmpp(boolean user_already_created, String username, String username2) {
        this.user_already_created = user_already_created;
        this.username = username;
        this.username2 = username2;
    }

    public void run() {
        try {
            serverConnect();
            if(user_already_created)logintoServer(username, "pass", username2);
            else {
                createAccount(username, "pass");
                logintoServer(username, "pass", username2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private XMPPTCPConnectionConfiguration buildConfiguration() throws XmppStringprepException, UnknownHostException {
        XMPPTCPConnectionConfiguration.Builder builder = XMPPTCPConnectionConfiguration.builder();

        builder.setHostAddress(InetAddress.getByName(HOST));
        builder.setPort(PORT);
        builder.setCompressionEnabled(false);
        //builder.setDebuggerFactory();
        builder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        builder.setSendPresence(true);
        DomainBareJid serviceName = JidCreate.domainBareFrom("myserver");
        builder.setXmppDomain(serviceName);

        return builder.build();
    }

    public void serverConnect() throws IOException, InterruptedException, XMPPException, SmackException {
        XMPPTCPConnectionConfiguration config = buildConfiguration();
        SmackConfiguration.DEBUG = true;
        this.connection = new XMPPTCPConnection(config);
        this.connection.connect();
    }

    public void createAccount(String user, String pass) throws XmppStringprepException, XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        SASLAuthentication.unBlacklistSASLMechanism("PLAIN");
        SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
        AccountManager accountManager = AccountManager.getInstance(connection);
        accountManager.sensitiveOperationOverInsecureConnection(true);
        accountManager.createAccount(Localpart.from(user), pass);
    }

    public void logintoServer(String user, String pass, String user2) throws InterruptedException, IOException, SmackException, XMPPException, JSONException {
        SASLAuthentication.unBlacklistSASLMechanism("PLAIN");
        SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
        connection.login(Localpart.from(user), pass);

        //tutaj jest wymiana aktualnej lokalizacji między uzytkownikami
        //zamiast podanych na sztywno liczb o kluczach "longitude" i "latitude" należy pobierać swoją aktualną lokalizację (rozumiem, że będzie to zrobione przez google maps) i ją wysyłać
        ChatManager chatManager = ChatManager.getInstanceFor(connection);
        chatManager.addIncomingListener(new IncomingChatMessageListener() {
            @Override
            public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
                try {
                    js2 = new JSONObject(message.getBody());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("New message from " + from + ": " + message.getBody());
            }
        });

        EntityBareJid jid = JidCreate.entityBareFrom(user2 + "@myserver");
        Chat chat = chatManager.chatWith(jid);
        while(connection.isConnected()) {
            JSONObject js = new JSONObject();
            js.put("type", "location");
            js.put("longitude", 78.3483);
            js.put("latitude", 42.5383);
            chat.send(String.valueOf(js));
            sleep(2000);
        }
    }
}
