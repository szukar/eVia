package com.example.xmppexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void loginClick(View view) {
        EditText editText = findViewById(R.id.editText2);
        EditText editText2 = findViewById(R.id.editText);
        String username = editText.getText().toString();
        String username2 = editText2.getText().toString();

        Intent intent = new Intent(getApplicationContext(), SecondWindow.class);
        Bundle b = new Bundle();
        b.putString("key", username); //Your id
        b.putString("key2", username2);
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
        finish();

    }
}
