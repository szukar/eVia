package com.example.xmppexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class SecondWindow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_window);

        Bundle b = getIntent().getExtras();
        String username = "";
        String username2 = "";
        if(b != null) {
            username = b.getString("key");
            username2 = b.getString("key2");
        }
        //tutaj zamiast pobierac nazwe uzytkownika z wpisania loginu, to bedzie ona pobierana z facebooka

        boolean user_already_created = false;
        //W tym momencie trzeba sprawdzić czy dana nazwa uzytkownika istnieje w bazie i jesli tak to zmienna user_already_created = true;

        //następnie uzytkownik będzie wyszukiwal w bazie najblizszego kierowce i przekazywal jego nazwę do klasy Xmpp jako String (username2), aby móc wysyłać do niego swoją lokalizację

        Xmpp xmpp = new Xmpp(user_already_created, username, username2);
        xmpp.start();
    }
}
