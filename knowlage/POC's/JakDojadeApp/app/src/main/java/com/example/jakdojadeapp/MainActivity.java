package com.example.jakdojadeapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    Button b;
    EditText x, y;
    LocationManager lm;
    Criteria kr;
    Location loc;
    String najlepszyDostawca;
    WebView wv;
    int MY_PERMISSIONS_REQUEST;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        if (/*ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || */ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

            //ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST);
           //ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST);
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.INTERNET},MY_PERMISSIONS_REQUEST);
            return;
        }
        /*  kr = new Criteria();
            lm = (LocationManager) getSystemService(LOCATION_SERVICE);
            najlepszyDostawca = lm.GPS_PROVIDER;
            loc = lm.getLastKnownLocation(najlepszyDostawca);
        */

        b = findViewById(R.id.button);
        x = findViewById(R.id.wspX);
        y = findViewById(R.id.wspY);
        wv = findViewById(R.id.webview);



        View.OnClickListener l = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                otworzJakDojade();
            }
        };
        b.setOnClickListener(l);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void otworzJakDojade() {
        runOnUiThread(new Runnable(){
            public void run() {
                //pobranie daty z telefonu
                Date currentTime = Calendar.getInstance().getTime();
                //ustawienie formatu daty
                SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yy");
                String date = format1.format(Date.parse(currentTime.toString()));
                //ustawienie formatu casu
                SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
                String time = format2.format(Date.parse(currentTime.toString()));

                //enable JavaScript
                WebSettings webSettings = wv.getSettings();
                webSettings.setJavaScriptEnabled(true);
                //dzieki temu działają zwracane przez JakDojade trasy
                wv.getSettings().setDomStorageEnabled(true);

                wv.setVisibility(View.VISIBLE);
                b.setVisibility(View.INVISIBLE);
                x.setVisibility(View.INVISIBLE);
                y.setVisibility(View.INVISIBLE);
                //wsp akademika w toruniu
                double test1 = 53.012444;
                double test2 = 18.595078;


                String url = "https://jakdojade.pl/torun/trasa/z--undefined--do--undefined?tc="+x.getText()+":"+y.getText()+"&fc="+test1+":"+test2+"&ft=LOCATION_TYPE_COORDINATE&tt=LOCATION_TYPE_COORDINATE&d="+date+"&h="+time+"&aro=1&t=1&rc=3&ri=1&r=0";
                wv.loadUrl(url);


            }
        });
    }
}
