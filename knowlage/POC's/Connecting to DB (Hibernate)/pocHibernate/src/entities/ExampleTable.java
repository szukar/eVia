package entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Objects;

public class ExampleTable {
    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    private String exampleString;
    private Integer exampleInteger;

    public ExampleTable() {
        super();
    }

    public ExampleTable(String exampleString, Integer exampleInteger) {
        this.exampleString = exampleString;
        this.exampleInteger = exampleInteger;
    }

    public ExampleTable(Integer id) {
        this.id = id;
    }

    public ExampleTable(Integer id, String exampleString, Integer exampleInteger) {
        this.id = id;
        this.exampleString = exampleString;
        this.exampleInteger = exampleInteger;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExampleString() {
        return exampleString;
    }

    public void setExampleString(String string) {
        this.exampleString = string;
    }

    public Integer getExampleInteger() {
        return exampleInteger;
    }

    public void setExampleInteger(Integer integer) {
        this.exampleInteger = integer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExampleTable exampleTable = (ExampleTable) o;

        if (!Objects.equals(id, exampleTable.id)) return false;
        if (!Objects.equals(exampleString, exampleTable.exampleString)) return false;
        return Objects.equals(exampleInteger, exampleTable.exampleInteger);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (exampleString != null ? exampleString.hashCode() : 0);
        result = 31 * result + (exampleInteger != null ? exampleInteger.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Table{" +
                "id=" + id +
                ", string='" + exampleString + '\'' +
                ", integer=" + exampleInteger +
                '}';
    }
}
