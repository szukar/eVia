import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;

import entities.ExampleTable;

import java.util.List;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    private static void insertOrUpdate(ExampleTable entity) {
        final Session session = getSession();
        Transaction tx;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(entity); //jeżeli istnieje obiekt w bazie o id takim samym jak row.id to nadpisuje jak nie to wstawia do bazy
            tx.commit();
        } finally {
            session.close();
        }
    }

    private static void delete(ExampleTable entity) {
        final Session session = getSession();
        Transaction tx;
        try {
            tx = session.beginTransaction();
            session.delete(entity); //wystarczy aby zgadzało zię id
            tx.commit();
        } finally {
            session.close();
        }
    }

    private static List<ExampleTable> list() {
        final Session session = getSession();
        Transaction tx;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(ExampleTable.class); //jest to metoda oznaczona jako niewspierana, ale ciągle działa i jest bardzo przydatna
            List<ExampleTable> list = criteria.list();
            tx.commit();

            return list;
        } finally {
            session.close();
        }
    }

    private static List<ExampleTable> queryByExample(ExampleTable exampleEntity) {
        final Session session = getSession();
        Transaction tx;
        try {
            tx = session.beginTransaction();
            Example example = Example.create(exampleEntity);
            Criteria criteria = session.createCriteria(ExampleTable.class).add(example); //jest to metoda oznaczona jako niewspierana, ale ciągle działa i jest bardzo przydatna
            List<ExampleTable> list = criteria.list();
            tx.commit();

            return list;
        } finally {
            session.close();
        }
    }

    public static void main(final String[] args) throws Exception {
        //listowanie tabeli
        System.out.println("list");
        for (ExampleTable t : list()) {
            System.out.println(t);
        }

        //przykładowe obiekty
        ExampleTable example1 = new ExampleTable("teststring1", 5);
        ExampleTable example2 = new ExampleTable("teststring2", 7);

        //dodawanie do tabeli
        System.out.println("insertOrUpdate");
        insertOrUpdate(example1);
        insertOrUpdate(example2);

        System.out.println("list");
        for (ExampleTable t : list()) {
            System.out.println(t);
        }

        //szukanie po przykładzie (to samo id)
        System.out.println("obiekt przed nadpisaniem");
        ExampleTable example3 = list().get(0);
        System.out.println(example3);

        System.out.println("nadpisanie obiektu w bazie");
        example3.setExampleString("zmieniony string obiektu");
        insertOrUpdate(example3);

        System.out.println("list");
        for (ExampleTable t : list()) {
            System.out.println(t);
        }

        //usuwanie z tabeli
        System.out.println("delete");
        for (ExampleTable t : list()) {
            delete(t);
        }

        System.out.println("list");
        for (ExampleTable t : list()) {
            System.out.println(t);
        }
    }
}