package eviateam.evia.utility;

import android.content.Context;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class UserRoleShiftClass
{
    public void userRoleShiftToPassenger(Context context) throws IOException
    {

        try (OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput("evia_role.txt", Context.MODE_PRIVATE)))
            {
                writer.write(1);
            }
    }

    public void userRoleShiftToDriver(Context context) throws IOException
    {

        try (OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput("evia_role.txt", Context.MODE_PRIVATE)))
            {
                writer.write(0);
            }

    }

}
